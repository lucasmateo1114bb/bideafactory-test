-Para crear el contenedor con Mongo realizar lo siguiente:
[Al ser mongo una imagen oficial es probable que ponga problema al realizar el pull de esta y pida docker login.]
docker create --name mongo-container -p 27017:27017 mongo
-Luego necesitamos iniciarla
docker start mongo-container
Con esto ya tenemos nuestra mongodb escuchando por le puerto 27017

-Validar información en MongoDB
Abrimos la consola del contenedor
docker exec -it mongo-container bash
Entramos al Mongo Shell
mongo
Listamos las bases de datos y debemos encontrar financial_indicators que fue la que definimos en application.properties
db.adminCommand({listDatabases : 1})
Nos pasamos a la db financial_indicators
use financial_indicators
Listamos las colecciones y debemos encontrar la colección indicators
db.getCollectionNames()
Verificamos que se encuentre guardando las peticiones a api.desarrolladores.datos.gob.cl
db.indicator.find()
