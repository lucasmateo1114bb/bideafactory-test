package com.bideafactory.api;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.bideafactory.constants.Constants;
import com.bideafactory.db.entity.Indicator;
import com.bideafactory.db.repository.IndicatorRepository;
import com.bideafactory.model.IndicatorResponseDolarVO;
import com.bideafactory.model.IndicatorResponseUfVO;
import com.bideafactory.model.IndicatorResponseUtmVO;
import com.bideafactory.model.IndicatorValuesVO;
import com.bideafactory.service.FinancialIndicatorsConsumer;

/**
 * @author Lucas Sánchez
 * 
 * This class is used to test FinancialIndicatorsController class.
 *
 */
@RunWith(SpringRunner.class)
@WebMvcTest(value = FinancialIndicatorsController.class)
public class FinancialIndicatorsControllerTest {
	
	/**
	 * Used to test controllers.
	 */
	@Autowired
	private MockMvc mockMvc;
	
	/**
	 * IndicatorRepository Mock.
	 */
	@MockBean
	private IndicatorRepository indicatorRepository;
	
	/**
	 * FinancialIndicatorsConsumer Mock.
	 */
	@MockBean
	private FinancialIndicatorsConsumer indicatorsConsumer;
	
	/**
	 * This method test response when indicators are on MongoDB.
	 * 
	 * @throws Exception
	 */
	@Test
	public void getFinancialIndicatorsResponseFromMongoTest() throws Exception{
		Indicator mockIndicatorItem = new Indicator();
		mockIndicatorItem.dolar = 822.93f;
		mockIndicatorItem.uf = 28716.52f;
		mockIndicatorItem.utm =	50.372f;
		mockIndicatorItem.id = "5099803df3f4948bd2f98391";
		mockIndicatorItem.fecha = "2020-05-15";
		
		Mockito.when(indicatorRepository.findByFecha(Mockito.anyString())).thenReturn(mockIndicatorItem);
		
		RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/bideafactory/indicadoreshoy").accept(MediaType.APPLICATION_JSON);
		
		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		
		JSONAssert.assertEquals("{\"indicators\":{\"dolar\":822.93,\"uf\":28716.52,\"utm\":50.372},\"date\":\"2020-05-15\"}", result.getResponse()
				.getContentAsString(), false);		
	}
	
	/**
	 * This method test response when indicators are no on MongoDB.
	 * 
	 * @throws Exception
	 */
	@Test
	public void getFinancialIndicatorsResponseFromServiceTest() throws Exception{		
		IndicatorResponseDolarVO mockDolarIndicator = new IndicatorResponseDolarVO();
		IndicatorValuesVO valuesDolar = new IndicatorValuesVO();
		valuesDolar.setFecha("2020-05-15");
		valuesDolar.setValor(822.93f);
		mockDolarIndicator.setApiVersion("1.0");
		mockDolarIndicator.setDolar(valuesDolar);
		
		IndicatorResponseUfVO mockUfIndicator = new IndicatorResponseUfVO();
		IndicatorValuesVO valuesUf = new IndicatorValuesVO();
		valuesUf.setFecha("2020-05-15");
		valuesUf.setValor(28716.52f);
		mockUfIndicator.setApiVersion("1.0");
		mockUfIndicator.setUf(valuesUf);
		
		IndicatorResponseUtmVO mockUtmIndicator = new IndicatorResponseUtmVO();
		IndicatorValuesVO valuesUtm = new IndicatorValuesVO();
		valuesUtm.setFecha("2020-05-15");
		valuesUtm.setValor(50.372f);
		mockUtmIndicator.setApiVersion("1.0");
		mockUtmIndicator.setUtm(valuesUtm);
		
		Mockito.when(indicatorRepository.findByFecha(Mockito.anyString())).thenReturn(null);
		Mockito.when(indicatorRepository.save(Mockito.any())).thenReturn(null);
		
		Mockito.when(indicatorsConsumer.getIndicator(Constants.DOLAR_INDICATOR)).thenReturn(mockDolarIndicator);
		Mockito.when(indicatorsConsumer.getIndicator(Constants.UF_INDICATOR)).thenReturn(mockUfIndicator);
		Mockito.when(indicatorsConsumer.getIndicator(Constants.UTM_INDICATOR)).thenReturn(mockUtmIndicator);
		
		RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/bideafactory/indicadoreshoy").accept(MediaType.APPLICATION_JSON);
		
		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		
		System.out.println(result);
		
		JSONAssert.assertEquals("{\"indicators\":{\"dolar\":822.93,\"uf\":28716.52,\"utm\":50.372},\"date\":\"2020-05-15\"}", result.getResponse()
				.getContentAsString(), false);		
	}
	
	/**
	 * This method test the correct response when something is wrong.
	 * 
	 * In this case we are provoking an exception to get Invalid Request as response.
	 * 
	 * @throws Exception
	 */
	@Test
	public void getFinancialIndicatorsBadRequest() throws Exception{
		
		
		Mockito.when(indicatorRepository.findByFecha(Mockito.anyString())).thenReturn(null);
		Mockito.when(indicatorRepository.save(Mockito.any())).thenReturn(null);
		
		RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/bideafactory/indicadoreshoy").accept(MediaType.APPLICATION_JSON);
		
		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		
		System.out.println(result);
		
		JSONAssert.assertEquals("{\"code\":400,\"message\":\"Invalid Request\"}", result.getResponse()
				.getContentAsString(), false);		
	}
}
