package com.bideafactory.db.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.bideafactory.db.entity.Indicator;

/**
 * 
 * @author Lucas Sánchez
 * 
 * This interface manage indicator collection
 *
 */
public interface IndicatorRepository extends MongoRepository<Indicator, String>{
	
	/**
	 * Find a record by fecha from indicator collection
	 * 
	 * @param fecha
	 * @return
	 */
	public Indicator findByFecha(String fecha);
}
