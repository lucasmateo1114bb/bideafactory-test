package com.bideafactory.db.entity;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * 
 * @author Lucas Sánchez
 *
 * Indicator Entity.
 *
 */
@Document(collection = "indicators")
public class Indicator {
	
	/**
	 * Id field.
	 */
	@Id
	public String id;
	
	/**
	 * Fecha field.
	 */
	public String fecha;
	
	/**
	 * Dolar field.
	 */
	public float dolar;
	
	/**
	 * Uf field.
	 */
	public float uf;
	
	/**
	 * Utm field.
	 */
	public float utm;
	
	/**
	 * Indicator constructor
	 */
	public Indicator() {
		
	}
	
	/**
	 * Function to show record as string.
	 */
	public String toString() {
		return String.format("Indicator[id=%s, fecha='%s', dolar=%s, uf=%s, utm=%s]", 
		id, fecha, dolar, uf, utm);
	}
}
