package com.bideafactory;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 
 * @author Lucas Sánchez
 *
 */
@SpringBootApplication
public class BideafactoryTestApplication {

	public static void main(String[] args) {
		SpringApplication.run(BideafactoryTestApplication.class, args);
	}

}
