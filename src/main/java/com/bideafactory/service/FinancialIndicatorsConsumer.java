package com.bideafactory.service;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.bideafactory.constants.Constants;
import com.bideafactory.model.IndicatorResponseDolarVO;
import com.bideafactory.model.IndicatorResponseUfVO;
import com.bideafactory.model.IndicatorResponseUtmVO;
import com.bideafactory.model.IndicatorResponseVO;

import ch.qos.logback.classic.Logger;

/**
 * 
 * @author Lucas Sánchez
 * 
 * This class is used to communication between our API and  datos gob cl.
 */
@Service
public class FinancialIndicatorsConsumer {
	
	/**
	 * Logger
	 */
	private Logger logger = (Logger) LoggerFactory.getLogger(FinancialIndicatorsConsumer.class);

	/**
	 * This function consume get services according to the indicator received.
	 * 
	 * @param pIndicator
	 * @return IndicatorResponseVO object
	 * @throws Exception
	 */
	public IndicatorResponseVO getIndicator(String pIndicator) throws Exception {
		logger.info("FinancialIndicatorsConsumer:getIndicator Consuming service to get "+pIndicator+" indicator");
		IndicatorResponseVO indicatorResult = null;
		
		ExecutorService executorService = Executors.newSingleThreadExecutor();		 
        /*
         * Do the call in a separate thread
         */
        Future<IndicatorResponseVO> future = executorService.submit(()-> {         	
        	IndicatorResponseVO indicatorResponse = null;
    		int maxRetries = Constants.MAX_RETRIES;
    		int retries = 1;
        	while(true) {
    			if(retries<=maxRetries) {
    				logger.debug("FinancialIndicatorsConsumer:getIndicator Attemp "+retries);
    				try {
    					RestTemplate restTemplate = new RestTemplate();
    			
    					/*
    					 * Replace values based on indicator.
    					 */
    					String url = Constants.URL_SERVICE;
    					url = url.replace("authkey", Constants.DATOS_GOB_CL_AUTH_KEY);
    					url = url.replace("indicator", pIndicator);
    					
    					logger.debug("FinancialIndicatorsConsumer:getIndicator path to consume "+url);
    					if(pIndicator.equals(Constants.DOLAR_INDICATOR)) {
    						IndicatorResponseDolarVO indicatorResponseDolar = restTemplate.getForObject(url, IndicatorResponseDolarVO.class);
    						indicatorResponse = indicatorResponseDolar;
    					}else if(pIndicator.equals(Constants.UF_INDICATOR)) {
    						IndicatorResponseUfVO indicatorResponseUf= restTemplate.getForObject(url, IndicatorResponseUfVO.class);
    						indicatorResponse = indicatorResponseUf;
    					}else {
    						IndicatorResponseUtmVO indicatorResponseUtm = restTemplate.getForObject(url, IndicatorResponseUtmVO.class);
    						indicatorResponse = indicatorResponseUtm;
    					}			
    					break;
    				}catch(Exception e) {
    					logger.error("FinancialIndicatorsConsumer:getIndicator Error consuming service - attemp "+retries+"::: "+e.toString());
    				}
    				retries+=1;
    			}else {
    				logger.error("FinancialIndicatorsConsumer:getIndicator Error consuming service ::: Number of attempts exceeded");
    				throw new Exception("Number of attempts exceeded");
    			}			
    		}
    		return indicatorResponse ;	
    	});
        
        try {
            /*
             *  Wait for at most 5 seconds to throw TimeoutException
             */
        	indicatorResult = future.get(Constants.SERVICE_TIMEOUT_SECONDS, TimeUnit.SECONDS);            
        } catch (TimeoutException e) {
        	logger.error("FinancialIndicatorsConsumer:getIndicator this method call has exceeded "+Constants.SERVICE_TIMEOUT_SECONDS+" seconds ::: "+e.toString());
        } catch (InterruptedException | ExecutionException e) {
        	logger.error("FinancialIndicatorsConsumer:getIndicator An unexpected error has ocurred ::: "+e.toString());
        }
        return indicatorResult;
	}	
}
