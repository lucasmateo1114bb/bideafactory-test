package com.bideafactory.constants;

/**
 * 
 * @author Lucas Sánchez
 * 
 * Class to hold constants.
 *
 */
public class Constants {
	
	/**
	 * Hold desarrolladores.datos.gob.cl API Auth Key.
	 */
	public static final String DATOS_GOB_CL_AUTH_KEY= "WIHVqcywDM9psFQvQwQJB7kCar01jGjTgRTThR1e";
	
	/**
	 * Hold common URL to consume desarrolladores.datos.gob.cl services.
	 */
	public static final String URL_SERVICE = "https://api.desarrolladores.datos.gob.cl/indicadores-financieros/v1/indicator/hoy.json/?auth_key=authkey";
	
	/**
	 * Hold dolar indicator.
	 */
	public static final String DOLAR_INDICATOR = "dolar";
	
	/**
	 * Hold uf indicator
	 */
	public static final String UF_INDICATOR = "uf";
	
	/**
	 * Hold utm indicator.
	 */
	public static final String UTM_INDICATOR = "utm";
	
	/**
	 * Hold max retries number.
	 */
	public static final int MAX_RETRIES = 3;
	
	/**
	 * Hold max retries number.
	 */
	public static final int SERVICE_TIMEOUT_SECONDS = 5;
}
