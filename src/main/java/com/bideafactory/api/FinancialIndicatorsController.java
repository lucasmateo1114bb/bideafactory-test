package com.bideafactory.api;


import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bideafactory.db.entity.Indicator;
import com.bideafactory.db.repository.IndicatorRepository;
import com.bideafactory.model.ErrorResponse;
import com.bideafactory.model.IndicadoresResponse;
import com.bideafactory.model.IndicatorResponseDolarVO;
import com.bideafactory.model.IndicatorResponseUfVO;
import com.bideafactory.model.IndicatorResponseUtmVO;
import com.bideafactory.model.Indicators;
import com.bideafactory.model.Response;
import com.bideafactory.service.FinancialIndicatorsConsumer;

import ch.qos.logback.classic.Logger;

/**
 * 
 * @author Lucas Sánchez
 *
 * Financial Indicators API
 *
 */
@RestController
@RequestMapping("/bideafactory")
public class FinancialIndicatorsController {
	
	/**
	 * FinancialIndicatorsConsumer object to consume
	 * desarrolladores.datos.gob.cl services.
	 */
	@Autowired
	private FinancialIndicatorsConsumer indicatorsConsumer;
	
	/**
	 * IndicatorRepository object to manage indicator
	 * items.
	 */
	@Autowired
	private IndicatorRepository indicatorRepository;
	
	/**
	 * Logger
	 */
	private Logger logger = (Logger) LoggerFactory.getLogger(FinancialIndicatorsConsumer.class);
	
	/**
	 * This service return dolar, UF, UTM information of current day.
	 * 
	 * @param pRequest
	 * @param pResponse
	 * @return
	 */
	@GetMapping(path = "/indicadoreshoy", produces = "application/json")
	public Response getFinancialIndicators(HttpServletRequest pRequest, HttpServletResponse pResponse) {
		logger.info("FinancialIndicatorsController:getFinancialIndicators Entering...");
		try {
			pResponse.setStatus(HttpServletResponse.SC_OK);
			LocalDate today = LocalDate.now();
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("YYYY-MM-dd");
			String todayString = formatter.format(today);
			
			/*
			 *	Find indicators from MongoDB based on current day.  
			 */
			Indicator indicator = indicatorRepository.findByFecha(todayString);
			
			IndicatorResponseDolarVO indicatorResponseDolarVO;
			IndicatorResponseUfVO indicatorResponseUfVO;
			IndicatorResponseUtmVO indicatorResponseUtmVO;
			Indicators indicators;
			IndicadoresResponse indicadoresResponse;
			Indicator newIndicator;
			
			if(indicator==null){
				logger.debug("FinancialIndicatorsController:getFinancialIndicators indicators are not on MongoDB");
				/*
				 * This means MongoDB doest not have current indicators record 
				 * so we need to request it to api.desarrolladores.datos.gob.cl.
				 */
				indicatorResponseDolarVO = (IndicatorResponseDolarVO) indicatorsConsumer.getIndicator("dolar");
				indicatorResponseUfVO = (IndicatorResponseUfVO) indicatorsConsumer.getIndicator("uf");
				indicatorResponseUtmVO = (IndicatorResponseUtmVO) indicatorsConsumer.getIndicator("utm");
				
				/*
				 * Preparing response according to object IndicadoresResponse
				 */
				indicators = new Indicators();				
				indicators.setDolar(indicatorResponseDolarVO.getDolar().getValor());
				indicators.setUf(indicatorResponseUfVO.getUf().getValor());
				indicators.setUtm(indicatorResponseUtmVO.getUtm().getValor());
				
				indicadoresResponse = new IndicadoresResponse();
				indicadoresResponse.setDate(indicatorResponseDolarVO.getDolar().getFecha());
				indicadoresResponse.setIndicators(indicators);
				
				newIndicator = new Indicator();
				newIndicator.fecha = indicatorResponseDolarVO.getDolar().getFecha();
				newIndicator.dolar = indicatorResponseDolarVO.getDolar().getValor();
				newIndicator.uf = indicatorResponseUfVO.getUf().getValor();
				newIndicator.utm = indicatorResponseUtmVO.getUtm().getValor();
				
				/*
				 * This condition in case api.desarrolladores.datos.gob.cl does not have 
				 * indicators of current day yet. This condition avoid duplicates.
				 */
				if(todayString.equals(indicatorResponseDolarVO.getDolar().getFecha())) {
					logger.debug("FinancialIndicatorsController:getFinancialIndicators saving indicators "+
					indicatorResponseDolarVO.getDolar().getFecha());
					indicatorRepository.save(newIndicator);
				}	
				return indicadoresResponse;				
			}else {
				logger.debug("FinancialIndicatorsController:getFinancialIndicators returning indicators from MongoDB");
				/*
				 *	We have indicators on mongo, now we need to adapt it to IndicadoresResponse. 
				 */
				indicators = new Indicators();	
				indicators.setDolar(indicator.dolar);
				indicators.setUf(indicator.uf);
				indicators.setUtm(indicator.utm);
				indicadoresResponse = new IndicadoresResponse();
				indicadoresResponse.setDate(indicator.fecha);
				indicadoresResponse.setIndicators(indicators);
				return indicadoresResponse;
			}			
		}catch(Exception e) {
			pResponse.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			logger.error("FinancialIndicatorsController:getFinancialIndicators Error::: "+e.toString());
			ErrorResponse errorResponse = new ErrorResponse();
			errorResponse.setCode(400);
			errorResponse.setMessage("Invalid Request");
			return errorResponse;
		}
	}
}
