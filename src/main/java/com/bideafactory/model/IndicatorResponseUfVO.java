package com.bideafactory.model;

/**
 * 
 * @author Lucas Sánchez
 *
 * This class represents response from uf indicator service.
 */
public class IndicatorResponseUfVO implements IndicatorResponseVO{
	
	/**
	 * Uf indicator values.
	 */
	private IndicatorValuesVO mUf;
	
	/**
	 * API Version.
	 */
	private String mApiVersion;
	
	/**
	 * Timestamp.
	 */
	private long mTimestamp;

	/**
	 * Function to  get API Version
	 * 
	 * @return
	 */
	public String getApiVersion() {
		return mApiVersion;
	}

	/**
	 * Method to set API Version
	 * 
	 * @param pApiVersion
	 */
	public void setApiVersion(String pApiVersion) {
		this.mApiVersion = pApiVersion;
	}

	/**
	 * Function to get timestamp.
	 * 
	 * @return
	 */
	public long getTimestamp() {
		return mTimestamp;
	}

	/**
	 * Method to set timestamp.
	 * 
	 * @param pTimestamp
	 */
	public void setTimestamp(long pTimestamp) {
		this.mTimestamp = pTimestamp;
	}

	/**
	 * Function to get uf indicator values
	 * 
	 * @return
	 */
	public IndicatorValuesVO getUf() {
		return mUf;
	}

	/**
	 * Method to set uf indicator values.
	 * 
	 * @param pUf
	 */
	public void setUf(IndicatorValuesVO pUf) {
		this.mUf = pUf;
	}

}
