package com.bideafactory.model;

/**
 * 
 * @author Lucas Sánchez
 * 
 * This class represents response from utm indicator service.
 *
 */
public class IndicatorResponseUtmVO implements IndicatorResponseVO{
	
	/**
	 * Utm indicator values.
	 */
	private IndicatorValuesVO mUtm;
	
	/**
	 * API Version.
	 */
	private String mApiVersion;
	
	/**
	 * Timestamp.
	 */
	private long mTimestamp;

	/**
	 * Function to get API version.
	 * 
	 * @return
	 */
	public String getApiVersion() {
		return mApiVersion;
	}

	/**
	 * Method to set API version.
	 * 
	 * @param pApiVersion
	 */
	public void setApiVersion(String pApiVersion) {
		this.mApiVersion = pApiVersion;
	}

	/**
	 * Function to get timestamp.
	 * 
	 * @return
	 */
	public long getTimestamp() {
		return mTimestamp;
	}

	/**
	 * Method to set timestamp.
	 * 
	 * @param pTimestamp
	 */
	public void setTimestamp(long pTimestamp) {
		this.mTimestamp = pTimestamp;
	}

	/**
	 * Function to get utm indicator values.
	 * 
	 * @return
	 */
	public IndicatorValuesVO getUtm() {
		return mUtm;
	}

	/**
	 * Method to set utm indicator values.
	 * 
	 * @param pUtm
	 */
	public void setUtm(IndicatorValuesVO pUtm) {
		this.mUtm = pUtm;
	}

}
