package com.bideafactory.model;

/**
 * 
 * @author Lucas Sánchez
 *
 * This class represents a good response.
 */
public class IndicadoresResponse implements Response{
	
	/**
	 * Indicator date.
	 */
	private String mDate;
	
	/**
	 * Indicators
	 */
	private Indicators mIndicators;

	/**
	 * Function to get indicators.
	 * 
	 * @return
	 */
	public Indicators getIndicators() {
		return mIndicators;
	}

	/**
	 * Method to set indicators.
	 * 
	 * @param pIndicators
	 */
	public void setIndicators(Indicators pIndicators) {
		this.mIndicators = pIndicators;
	}

	/**
	 * Function to get date.
	 * 
	 * @return
	 */
	public String getDate() {
		return mDate;
	}

	/**
	 * Method to set date.
	 * 
	 * @param pDate
	 */
	public void setDate(String pDate) {
		this.mDate = pDate;
	}
}
