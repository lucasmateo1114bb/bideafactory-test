package com.bideafactory.model;

/**
 * 
 * @author Lucas Sánchez
 *
 * This class is used to represent service error response.
 */
public class ErrorResponse implements Response{
	
	/**
	 * Error code.
	 */
	private int mCode;
	
	/**
	 * Error message.
	 */
	private String mMessage;

	/**
	 * Function to get error code.
	 * 
	 * @return
	 */
	public int getCode() {
		return mCode;
	}

	/**
	 * Method to set error code.
	 * 
	 * @param pCode
	 */
	public void setCode(int pCode) {
		this.mCode = pCode;
	}

	/**
	 * Function to get error message.
	 * 
	 * @return
	 */
	public String getMessage() {
		return mMessage;
	}

	/**
	 * Method to set error message.
	 * 
	 * @param pMessage
	 */
	public void setMessage(String pMessage) {
		this.mMessage = pMessage;
	}	
}
