package com.bideafactory.model;

/**
 * 
 * @author Lucas Sánchez
 *
 * This class represents response from dolar indicator service.
 */
public class IndicatorResponseDolarVO implements IndicatorResponseVO{
	
	/**
	 * Dolar indicator.
	 */
	private IndicatorValuesVO mDolar;
	
	/**
	 * API Version.
	 */
	private String mApiVersion;
	
	/**
	 * Timestamp.
	 */
	private long mTimestamp;

	/**
	 * Function to get API version.
	 * 
	 * @return
	 */
	public String getApiVersion() {
		return mApiVersion;
	}

	/**
	 * Method to set API version.
	 * 
	 * @param pApiVersion
	 */
	public void setApiVersion(String pApiVersion) {
		this.mApiVersion = pApiVersion;
	}

	/**
	 * Function to get timestamp.
	 * 
	 * @return
	 */
	public long getTimestamp() {
		return mTimestamp;
	}

	/**
	 * Method to set timestamp.
	 * 
	 * @param pTimestamp
	 */
	public void setTimestamp(long pTimestamp) {
		this.mTimestamp = pTimestamp;
	}

	/**
	 * Function to get dolar indicator.
	 * 
	 * @return
	 */
	public IndicatorValuesVO getDolar() {
		return mDolar;
	}

	/**
	 * Method to set dolar indicator.
	 * 
	 * @param pDolar
	 */
	public void setDolar(IndicatorValuesVO pDolar) {
		this.mDolar = pDolar;
	}
	
	
}
