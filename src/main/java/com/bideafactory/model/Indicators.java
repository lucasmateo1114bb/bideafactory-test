package com.bideafactory.model;

/**
 * 
 * @author Lucas Sánchez
 *
 * This class represents indicators of IndicadoresResponse.java
 */
public class Indicators {
	
	/**
	 * Dolar indicator.
	 */
	private float mDolar; 
	
	/**
	 * Uf indicator.
	 */
	private float mUf;
	
	/**
	 * Utm indicator.
	 */
	private float mUtm;

	/**
	 * Function to get dolar indicator.
	 * 
	 * @return
	 */
	public float getDolar() {
		return mDolar;
	}

	/**
	 * Method to set dolar indicator.
	 * 
	 * @param pDolar
	 */
	public void setDolar(float pDolar) {
		this.mDolar = pDolar;
	}

	/**
	 * Function to get uf indicator.
	 * 
	 * @return
	 */
	public float getUf() {
		return mUf;
	}

	/**
	 * Method to set uf indicator.
	 * 
	 * @param pUf
	 */
	public void setUf(float pUf) {
		this.mUf = pUf;
	}

	/**
	 * Function to get utm indicator.
	 * 
	 * @return
	 */
	public float getUtm() {
		return mUtm;
	}

	/**
	 * Method to set utm indicator.
	 * 
	 * @param pUtm
	 */
	public void setUtm(float pUtm) {
		this.mUtm = pUtm;
	} 
}
