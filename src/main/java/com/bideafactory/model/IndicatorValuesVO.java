package com.bideafactory.model;

/**
 * 
 * @author Lucas Sánchez
 *
 * This class represents indicators values of response from indicators service.
 */
public class IndicatorValuesVO {
	
	/**
	 * Valor
	 */
	private float mValor;
	
	/**
	 * Fecha
	 */
	private String mFecha;

	/**
	 * Function to get valor.
	 * 
	 * @return
	 */
	public float getValor() {
		return mValor;
	}

	/**
	 * Method to set valor.
	 * 
	 * @param pValor
	 */
	public void setValor(float pValor) {
		this.mValor = pValor;
	}

	/**
	 * Function to get fecha.
	 * 
	 * @return
	 */
	public String getFecha() {
		return mFecha;
	}

	/**
	 * Method to set fecha.
	 * 
	 * @param pFecha
	 */
	public void setFecha(String pFecha) {
		this.mFecha = pFecha;
	}
}
